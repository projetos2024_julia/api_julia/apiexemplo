const router = require("express").Router();
const dbController = require("../controller/dbController");

router.get("/tablesName", dbController.getTablesName);
router.get("/tablesDescription", dbController.getTablesDescription);


module.exports = router;
